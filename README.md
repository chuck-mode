## About

A mode for the [ChucK] audio programming language.  This was written
in a fit of rage after discovering that [the existing mode made it
into the Emacs attic].  Based upon [CC Mode].

## Installation

Install manually or via [Quelpa] for the time being.

## Usage

Just code away.  There are no special features (like, automating
`chuck`) included yet...

## Alternatives

Use the [Emacs attic version][the existing mode made it into the Emacs
attic] for more highlighted UGens, `chuck` automation support, electric
operators and snippets.

[ChucK]: http://chuck.cs.princeton.edu/
[the existing mode made it into the Emacs attic]: https://github.com/emacsattic/chuck-mode
[CC Mode]: http://cc-mode.sourceforge.net/
[Quelpa]: https://github.com/quelpa/quelpa
